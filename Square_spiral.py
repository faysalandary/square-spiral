"""
Outputs the closest square spiral matrix of an input number
"""
from math import sqrt


def is_even_square(num):
    """
    True if an integer is even as well as a perfect square

    >>> is_even_square(25)
    False

    >>> is_even_square(64)
    True

    """
    return sqrt(num).is_integer() and (num % 2 == 0)


def find_nearest_square(num):
    """
    Returns the nearest even perfect square to a given integer

    >>> find_nearest_square(51)
    64

    >>> find_nearest_square(25)
    16

    """
    for i in range(num):
        if is_even_square(num - i):
            return num - i
        elif is_even_square(num + i):
            return num + i


def find_lower_squares(num):
    """
    Returns a list of even perfect squares less than a given integer

    >>> find_lower_squares(40)
    [36, 16, 4]
    
    """
    return [i for i in range(num, 3, -1) if is_even_square(i)]


def nth_row(num, n):
    """
    Returns the nth row of the square spiral matrix

    >>> nth_row(16,1)
    [5, 4, 3, 12]

    >>> nth_row(36,4)
    [20, 7, 8, 9, 10, 27]

    """
    edge = int(sqrt(num))
    squares = find_lower_squares(num)
    if n == 0:
        return list(range(num, num - edge, -1))
    elif n >= edge - 1:
        return list(range(num - 3*edge + 3, num - 2*edge + 3))
    elif n < edge // 2:
        return ([squares[1] + n] + nth_row(squares[1],n-1)
              + [num - edge - n + 1])
    else:
        return ([num - 3*edge + 4 + n - edge] + nth_row(squares[1],n-1)
              + [num - 2*edge + 1 - n + edge])


def generate_square_spiral(num):
    """
    Generates a square spiral matrix from a given integer

    >>> generate_square_spiral(36)
    [[36, 35, 34, 33, 32, 31], [17, 16, 15, 14, 13, 30], [18, 5, 4, 3, 12, 29], [19, 6, 1, 2, 11, 28], [20, 7, 8, 9, 10, 27], [21, 22, 23, 24, 25, 26]]

    """
    edge = int(sqrt(num))
    return [nth_row(num, row) for row in range(edge)]


def main ():
    num = None
    while not num:
        try:
            num = int(input('Input number: '))
        except ValueError:
            print('Invalid Number')
    nearest_square = find_nearest_square(num)
    matrix = generate_square_spiral(nearest_square)
    for row in range(len(matrix[0])):
        for col in range(len(matrix)):
            if matrix[row][col] < 10:
                print('  ',matrix[row][col],' ',sep='',end='')
            elif matrix[row][col] < 100:
                print(' ',matrix[row][col],' ',sep='',end='')
            else:
                print(matrix[row][col],' ',sep='',end='')
        print(2*"\n",end='')

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    main()
